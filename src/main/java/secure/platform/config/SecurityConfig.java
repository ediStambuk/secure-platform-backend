package secure.platform.config;

import org.springframework.http.HttpMethod;
import secure.platform.constants.Roles;
import secure.platform.contollers.VulnerabilityController;
import secure.platform.filter.JwtFilter;
import secure.platform.services.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private UserServiceImpl userServiceImpl;
    private JwtFilter jwtFilter;

    public SecurityConfig(UserServiceImpl userServiceImpl, JwtFilter jwtFilter) {
        this.userServiceImpl = userServiceImpl;
        this.jwtFilter = jwtFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/api/**").csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, VulnerabilityController.VULNERABILITY_API)
                .hasAuthority(Roles.PROFESOR)
                .antMatchers(HttpMethod.GET, VulnerabilityController.VULNERABILITY_API)
                .hasAnyAuthority(Roles.PROFESOR, Roles.STUDENT);

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userServiceImpl);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
