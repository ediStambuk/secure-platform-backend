package secure.platform.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import secure.platform.constants.Roles;
import secure.platform.models.User;
import secure.platform.repositories.UserRepository;

@Component
public class LoadData implements CommandLineRunner {

    private final UserRepository userRepository;

    public LoadData(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        User user = new User("vukovic_m123", "komplicirana_sifra", Roles.PROFESOR);
        userRepository.save(user);
    }
}
