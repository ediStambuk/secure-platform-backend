package secure.platform.models;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import secure.platform.constants.Roles;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Entity
@Table(name = "user")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @Transient
    private List<GrantedAuthority> authorities;

    public User() {
    }

    public User(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.authorities = User.getArrayOfAuthorities(role);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        authorities = User.getArrayOfAuthorities(role);
        System.out.println(authorities);
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static List<GrantedAuthority> getArrayOfAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (Roles.PROFESOR.equals(role)) {
            authorities.add(new SimpleGrantedAuthority(Roles.STUDENT));
            authorities.add(new SimpleGrantedAuthority(Roles.PROFESOR));
        } else {
            authorities.add(new SimpleGrantedAuthority(Roles.STUDENT));
        }
        return authorities;
    }
}
