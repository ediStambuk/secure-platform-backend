package secure.platform.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import secure.platform.constants.Roles;
import secure.platform.models.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequest {

    private String username;
    private String password;

    public User convertToStudentUser() {
        if ("".contentEquals(username) || "".contentEquals(password)) {
            return null;
        }
        return new User(this.username, this.password, Roles.STUDENT);
    }
}