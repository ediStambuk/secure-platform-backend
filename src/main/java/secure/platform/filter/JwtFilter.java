package secure.platform.filter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import secure.platform.constants.Roles;
import secure.platform.helpers.JwtHelper;
import secure.platform.services.impl.UserServiceImpl;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Component
public class JwtFilter extends OncePerRequestFilter {

    private JwtHelper jwtHelper;
    private UserServiceImpl userServiceImpl;

    private static HashMap<String, String> authMap = new HashMap<>() {{
        put("GET#/api/vulnerability", Roles.STUDENT);
        put("POST#/api/vulnerability", Roles.PROFESOR);
        put("PUT#/api/vulnerability", Roles.PROFESOR);
        put("DELETE#/api/vulnerability", Roles.PROFESOR);
    }};

    public JwtFilter(JwtHelper jwtHelper, UserServiceImpl userServiceImpl) {
        this.jwtHelper = jwtHelper;
        this.userServiceImpl = userServiceImpl;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        String token = null;
        String userName = null;
        UserDetails userDetails = null;

        String requestUri = httpServletRequest.getRequestURI();
        String method = httpServletRequest.getMethod();

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            token = authorizationHeader.substring(7);
            userName = jwtHelper.extractUsername(token);
        }

        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            userDetails = userServiceImpl.loadUserByUsername(userName);
        }

        if (!isAllowed(userDetails, token, requestUri, method)) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    public boolean isAllowed(UserDetails userDetails, String token, String requestUri, String method) {
        if (requestUri != null) {
            if (!requestUri.startsWith("/api")) {
                return true;
            } else if (userDetails != null && token != null) {
                if (jwtHelper.validateToken(token, userDetails)) {
                    boolean authorizied = false;
                    Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)userDetails.getAuthorities();
                    for (GrantedAuthority authority: authorities) {
                        if (JwtFilter.authMap.get(method + "#" + requestUri) != null &&
                                JwtFilter.authMap.get(method + "#" + requestUri).matches(authority.getAuthority())) {
                            authorizied = true;
                        }
                    }
                    return authorizied;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
