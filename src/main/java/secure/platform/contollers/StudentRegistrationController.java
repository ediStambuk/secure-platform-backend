package secure.platform.contollers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import secure.platform.models.User;
import secure.platform.models.dto.AuthRequest;
import secure.platform.services.UserService;

@RestController
@RequestMapping(StudentRegistrationController.BASE_URL)
public class StudentRegistrationController {

    public static final String BASE_URL = "/registration";

    private final UserService userService;

    public StudentRegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity register(@RequestBody AuthRequest authRequest) {
        if (authRequest == null) {
            return ResponseEntity.badRequest().build();
        }

        User user = authRequest.convertToStudentUser();
        if (user == null) {
            return ResponseEntity.badRequest().build();
        }

        this.userService.saveUser(user);

        return ResponseEntity.ok().build();
    }


}
