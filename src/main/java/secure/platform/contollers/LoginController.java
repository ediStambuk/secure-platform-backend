package secure.platform.contollers;

import secure.platform.helpers.JwtHelper;
import secure.platform.models.dto.AuthRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(LoginController.BASE_URL)
public class LoginController {

    public static final String BASE_URL = "/login";

    private JwtHelper jwtHelper;
    private AuthenticationManager authenticationManager;

    public LoginController(JwtHelper jwtHelper, AuthenticationManager authenticationManager) {
        this.jwtHelper = jwtHelper;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping
    public String generateToken(@RequestBody AuthRequest authRequest) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getUsername(),
                        authRequest.getPassword()));

        return jwtHelper.generateToken(authRequest.getUsername());
    }
}
