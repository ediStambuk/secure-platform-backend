package secure.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurePlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurePlatformApplication.class, args);
    }

}
