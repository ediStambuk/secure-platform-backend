package secure.platform.services;

import secure.platform.models.User;

public interface UserService {

    void saveUser(User user);
}
